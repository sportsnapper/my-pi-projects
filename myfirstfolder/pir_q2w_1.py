#!/usr/bin/env python3
# Gjones
# based on example code from quick2wire.com

from quick2wire.gpio import pins, In, Out, Both, Rising, Falling
from quick2wire.selector import Selector, Timer
import time

button = pins.pin(0, direction=In, interrupt=Both)
led = pins.pin(1, direction=Out)
pir = pins.pin(3, direction=In, interrupt=Both)

with button, led, pir,\
    Selector() as selector:
    
    selector.add(button)
    selector.add(pir)
    
    print("ready")
    print "  Start time = %s " % ( time.strftime ('%X') )
    
    while True:
        selector.wait()
        
        if selector.ready == button:
            if button.value:
                led.value = 1
            else:
                led.value = 0
        elif selector.ready == pir:
			if pir.value:
				print ("Motion detected")
				led.value = 1
			else:
				led.value = 0	
				print "  PIR Ready"
  				print "  Ready time = %s " % ( time.strftime ('%X') )
			
