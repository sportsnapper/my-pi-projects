#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD

from subprocess import * 
from time import sleep, strftime
from datetime import datetime

lcd = Adafruit_CharLCD()
lcd.begin(16,1)

x = 1
y = 0
while x < 17:
    
    lcd.setCursor(16 - x,y)
    lcd.message('0123456789abcdefghijk')
    sleep(1)
    x = x + 1

sleep(2)
lcd.clear()